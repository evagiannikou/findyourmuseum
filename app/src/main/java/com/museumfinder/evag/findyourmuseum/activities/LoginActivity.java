package com.museumfinder.evag.findyourmuseum.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.User;
import com.museumfinder.evag.findyourmuseum.asynctask.MyFavorites;
import com.museumfinder.evag.findyourmuseum.asynctask.MyHistory;
import com.museumfinder.evag.findyourmuseum.asynctask.MyReviews;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import com.museumfinder.evag.findyourmuseum.asynctask.MyFavorites;
import com.museumfinder.evag.findyourmuseum.asynctask.MyHistory;
import com.museumfinder.evag.findyourmuseum.asynctask.MyReviews;

import static com.museumfinder.evag.findyourmuseum.MainActivity.user;

public class LoginActivity extends AppCompatActivity {

    String emailstr;
    String passwordstr;

    String enter_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toolbar toolbar;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        toolbar.setTitle(Html.fromHtml("<font color='#fffffff'>Login</font>"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView forgot_pwd = (TextView) findViewById(R.id.forgot_your_pwd);
        forgot_pwd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        LoginActivity.this);

                LayoutInflater inflater = LoginActivity.this.getLayoutInflater();

                /*final EditText email = (EditText) findViewById(R.id.enter_email);*/

                // set dialog message
                alertDialogBuilder
                        .setView(inflater.inflate(R.layout.forgot_pwd_layout, null))
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                /*enter_email = email.getText().toString();*/

                                /*SendEmail sendEmail;
                                sendEmail = new SendEmail();
                                sendEmail.execute();*/
                            }
                        })
                        .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        final FloatingActionButton button = (FloatingActionButton) findViewById(R.id.login_btn);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                if (v.getId() == R.id.login_btn){

                    EditText email = (EditText) findViewById(R.id.email_input);
                    EditText password = (EditText) findViewById(R.id.pwd_input);

                    emailstr = email.getText().toString();
                    passwordstr = password.getText().toString();

                    SearchUser searchUser;
                    searchUser = new SearchUser();
                    searchUser.execute();
                    /*---------------check if user exists in DB call AsyncTask -----------------------*/

                }

            }
        });

        /*------------------ sign up from login fragment ---------------------*/
        TextView sign_up = (TextView) findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SignUp = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(SignUp);
            }
        });
    }

    class SendEmail extends AsyncTask<String, String, String> {

        String msg = "email already exists";
        @Override
        protected String doInBackground(String... strings) {

            try{
                String link="http://" + MainActivity.host + "/my_museums/get_users.php";

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line;

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());

                //looping through all the elements in json array
                for (int i = 0; i < jsonArray.length(); i++) {

                    //getting json object from the json array
                    JSONObject obj = jsonArray.getJSONObject(i);

                    if (obj.getString("email").equals(enter_email)){

                        String pwd = obj.getString("password");

                        /* insert function to actually send email*/

                        return msg = "email sent successfully";
                    }
                }

                return msg;

            } catch(Exception e){
                Log.d("Exception:", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("email sent successfully")){
                Toast.makeText(getApplicationContext(), "An email was sent successfully", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), "The email you entered doesn't exist!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    class SearchUser extends AsyncTask<String, String, String> {

        String msg = "email doesn't exist";
        @Override
        protected String doInBackground(String... strings) {

            try{
                String link="http://" + MainActivity.host + "/my_museums/get_users.php";

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line;

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());

                //looping through all the elements in json array
                for (int i = 0; i < jsonArray.length(); i++) {

                    //getting json object from the json array
                    JSONObject obj = jsonArray.getJSONObject(i);

                    if (obj.getString("email").equals(emailstr)){
                            Log.i("message: ", "Email exists already");
                            msg = "Email exists already";

                            if (obj.getString("password").equals(passwordstr)){
                                MainActivity.user_id = Integer.parseInt(obj.getString("id"));

                                user = new User();
                                user.setId(Integer.parseInt(obj.getString("id")));
                                user.setName(obj.getString("name"));
                                user.setSurname(obj.getString("surname"));
                                user.setEmail(obj.getString("email"));
                                user.setpassword(obj.getString("password"));
                                user.setFb_logged(Integer.parseInt(obj.getString("fb_login")));
                                user.setGoogle_logged(Integer.parseInt(obj.getString("google_login")));

                                MyFavorites myFavorites;
                                myFavorites = new MyFavorites();
                                myFavorites.execute();

                                MyHistory myHistory;
                                myHistory = new MyHistory();
                                myHistory.execute();

                                MyReviews myReviews;
                                myReviews = new MyReviews();
                                myReviews.execute();
                            } else {
                                msg = "Wrong password";
                            }
                            break;
                    }
                }

                return msg;

            } catch(Exception e){
                Log.d("Exception:", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("email doesn't exist")){
                Toast.makeText(getApplicationContext(), "Email doesn't exist!", Toast.LENGTH_SHORT).show();
            }
            else {
                if (msg.equals("Wrong password")){
                    Toast.makeText(getApplicationContext(), "Wrong password", Toast.LENGTH_SHORT).show();
                }
                else{

                    MainActivity.logged_in = true;
                    Intent MainActivity = new Intent(getApplicationContext(), MainActivity.class);
                    MainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(MainActivity);

                }
            }
        }
    }
}
