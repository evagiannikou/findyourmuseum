package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.R;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;
import static com.museumfinder.evag.findyourmuseum.R.id;
import static com.museumfinder.evag.findyourmuseum.R.layout;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tickets extends Fragment {


    public Tickets() {
        // Required empty public constructor
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myInflatedView = inflater.inflate(layout.tickets_layout, container,false);

        LinearLayout linearLayout = (LinearLayout) myInflatedView.findViewById(id.tickets_layout);
        linearLayout.removeAllViews();

        if (place.isFromGoogle()){
            TextView textView = new TextView(getContext());
            textView.setPadding(52,60,52,5);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setText(R.string.not_available_info);

            linearLayout.addView(textView);
        }
        else{
            for (int i = 0 ; i < place.getTickets().size() ; i++ ){
                TextView title = new TextView(getContext());
                TextView price = new TextView(getContext());

                if ( i == 0){
                    title.setPadding(52,60,52,5);
                }
                else{
                    title.setPadding(52,0,52,5);
                }
                title.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                title.setText(place.getTickets().get(i).getType());
                title.setTextColor(Color.parseColor("#AFBB88"));
                title.setTextSize(16);
                title.setTypeface(null, Typeface.BOLD);

                linearLayout.addView(title);

                price.setPadding(52,0,52,30);
                price.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                price.setText(place.getTickets().get(i).getPrice());
                price.setTextColor(Color.parseColor("#808080"));
                price.setTextSize(16);

                linearLayout.addView(price);

            }
        }


        return myInflatedView;
    }

}
