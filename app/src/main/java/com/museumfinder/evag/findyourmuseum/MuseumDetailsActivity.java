package com.museumfinder.evag.findyourmuseum;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;

import com.museumfinder.evag.findyourmuseum.adapters.MyPagerAdapter;

/**
 * Created by Eva G on 4/5/2017.
 */

public class MuseumDetailsActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        Toolbar mToolBar;
        TabLayout mTabLayout;
        ViewPager mPager;
        MyPagerAdapter mAdapter;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.museum_details);
        setTitle(GetNearbyPlacesData.place.getName());
        setTitle(Html.fromHtml("<font color='#fffffff'>" + GetNearbyPlacesData.place.getName() + " </font>"));

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mTabLayout.setTabsFromPagerAdapter(mAdapter);

        mTabLayout.setupWithViewPager(mPager);
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }
}

