package com.museumfinder.evag.findyourmuseum.activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.User;
import com.museumfinder.evag.findyourmuseum.asynctask.MyFavorites;
import com.museumfinder.evag.findyourmuseum.asynctask.MyHistory;
import com.museumfinder.evag.findyourmuseum.asynctask.MyReviews;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, View.OnClickListener {

    LoginButton loginButton;
    CallbackManager callbackManager;

    String INFO = "INFO: ";
    String DEBUG = "FB login DEBUG: ";

    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;

    private static final String TAG = "SignIn";

    private static final int STATE_SIGNED_IN = 0;
    private static final int STATE_SIGN_IN = 1;
    private static final int STATE_IN_PROGRESS = 2;
    private int mSignInProgress;

    private static final int RC_SIGN_IN = 0;

    int countUsers;

    User temp_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toolbar toolbar;
        SignInButton google_login_btn;
        FloatingActionButton button;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.sign_up_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        toolbar.setTitle(Html.fromHtml("<font color='#fffffff'>My profile</font>"));
        toolbar.setLogo(R.drawable.ic_person_white);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        button = (FloatingActionButton) findViewById(R.id.sign_up_btn);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                if (v.getId() == R.id.sign_up_btn) {

                    EditText name = (EditText) findViewById(R.id.name_input);
                    EditText surname = (EditText) findViewById(R.id.surname_input);
                    EditText email = (EditText) findViewById(R.id.email_input);
                    EditText password = (EditText) findViewById(R.id.pwd_input);


                    /*---------------insert to DB -----------------------*/
                    temp_user = new User();

                    temp_user.setName(name.getText().toString());
                    temp_user.setSurname(surname.getText().toString());
                    temp_user.setEmail(email.getText().toString());
                    temp_user.setpassword(password.getText().toString());
                    temp_user.setFb_logged(0);
                    temp_user.setGoogle_logged(0);

                    String validationError = temp_user.validateInputs(temp_user.getName(), temp_user.getSurname(), temp_user.getEmail(), temp_user.getpassword());

                    if (!validationError.equals("valid input")) {
                        Toast.makeText(getApplicationContext(), validationError, Toast.LENGTH_LONG).show();
                    }
                    else {
                        SignUpActivity.SearchUser searchUser;
                        searchUser = new SearchUser();
                        searchUser.execute();
                    }
                }

            }
        });


        /* ----------------------FACEBOOK login -------------------------- */
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.fb_login_btn);
        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(DEBUG, "Successful login with fb");

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            public void setProfileToView(JSONObject profileToView) {
                                try {

                                    Log.i(INFO, profileToView.getString("email"));

                                    temp_user = new User();
                                    temp_user.setName(profileToView.getString("first_name"));
                                    temp_user.setSurname(profileToView.getString("last_name"));
                                    temp_user.setEmail(profileToView.getString("email"));
                                    temp_user.setpassword("*******");
                                    temp_user.setFb_logged(1);
                                    temp_user.setGoogle_logged(0);

                                    String validationError = temp_user.validateInputs(temp_user.getName(), temp_user.getSurname(), temp_user.getEmail(), temp_user.getpassword());

                                    if (!validationError.equals("valid input")) {
                                        Toast.makeText(getApplicationContext(), validationError, Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        SignUpActivity.SearchUser searchUser;
                                        searchUser = new SearchUser();
                                        searchUser.execute();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.v("LoginActivity", response.getRawResponse());
                                Log.v("AccessToken", AccessToken.getCurrentAccessToken().getToken());
                                setProfileToView(object);

                                MainActivity.logged_in = true;
                                Intent MainActivity = new Intent(getApplicationContext(), MainActivity.class);
                                MainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(MainActivity);
                            }

                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                Log.d(DEBUG, "Cancelled login with fb");
                Toast.makeText(getApplicationContext(), "Login cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(DEBUG, "Failed login with fb");
                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
            }
        });
        /*------------------------------------------------------------------*/



        /*-----------------------Google sign in-----------------------------*/

        google_login_btn = (SignInButton) findViewById(R.id.google_login_btn);

        gso = buildSignInOptions();
        mGoogleApiClient = buildApiClient();

        google_login_btn.setOnClickListener(this);

        /*------------------------------------------------------------------*/
    }


    GoogleSignInOptions buildSignInOptions() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
    }

    GoogleApiClient buildApiClient() {
        return new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.google_login_btn:
                signIn();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected");
        mSignInProgress = STATE_SIGNED_IN;

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        Log.i(TAG, "onConnectionSuspended" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        PendingIntent mSignIntent;
        int mSignInError;

        Log.i(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());

        if (mSignInProgress != STATE_IN_PROGRESS) {
            mSignIntent = connectionResult.getResolution();
            mSignInError = connectionResult.getErrorCode();

            if (mSignInProgress == STATE_SIGN_IN) {
                signIn();
            }
        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            //If not request code is RC_SIGN_IN it must be facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            temp_user = new User();
            temp_user.setName(acct.getGivenName());
            temp_user.setSurname(acct.getFamilyName());
            temp_user.setEmail(acct.getEmail());
            temp_user.setpassword("*******");
            temp_user.setFb_logged(0);
            temp_user.setGoogle_logged(1);

            String validationError = temp_user.validateInputs(temp_user.getName(), temp_user.getSurname(), temp_user.getEmail(), temp_user.getpassword());

            if (!validationError.equals("valid input")) {
                Toast.makeText(getApplicationContext(), validationError, Toast.LENGTH_LONG).show();
            }
            else {
                SignUpActivity.SearchUser searchUser;
                searchUser = new SearchUser();
                searchUser.execute();
            }
        }
    }

    class SearchUser extends AsyncTask<String, String, String> {

        String msg = "email doesn't exist";

        @Override
        protected String doInBackground(String... strings) {

            try {
                String link = "http://" + MainActivity.host + "/my_museums/get_users.php";

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());

                countUsers = jsonArray.length();

                //looping through all the elements in json array
                for (int i = 0; i < jsonArray.length(); i++) {

                    //getting json object from the json array
                    JSONObject obj = jsonArray.getJSONObject(i);

                    if (obj.getString("email").equals(temp_user.getEmail())) {
                        Log.i("message: ", "Email exists already");
                        msg = "Email exists already";
                        break;
                    }
                }

                return msg;

            } catch (Exception e) {
                Log.d("Exception:", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("email doesn't exist")) {
                Log.i(TAG, "Insert into db new entry");

                SignUpActivity.InsertUser insertUser;
                insertUser = new InsertUser();
                insertUser.execute();

            } else {
                Toast.makeText(getApplicationContext(), "Email already exists!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    class InsertUser extends AsyncTask<String, String, String> {
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {
            try {

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("id", countUsers + 1);
                params.put("name", temp_user.getName());
                params.put("surname", temp_user.getSurname());
                params.put("email", temp_user.getEmail());
                params.put("password", temp_user.getpassword());
                params.put("fb_login", temp_user.getFb_logged());
                params.put("google_login", temp_user.getGoogle_logged());

                String link = "http://" + MainActivity.host + "/my_museums/insert_user.php";

                URL url = new URL(link);


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0;) {
                    System.out.print((char) c);
                }

                return "Inserted into db successfully";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }


        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("Inserted into db successfully")) {
                Log.i(TAG, "Inserted into db successfully");

                MainActivity.user_id = countUsers + 1;

                MainActivity.user = new User();
                MainActivity.user.setId(countUsers + 1);
                MainActivity.user.setName(temp_user.getName());
                MainActivity.user.setSurname(temp_user.getSurname());
                MainActivity.user.setEmail(temp_user.getEmail());
                MainActivity.user.setpassword(temp_user.getpassword());
                MainActivity.user.setFb_logged(temp_user.getFb_logged());
                MainActivity.user.setGoogle_logged(temp_user.getGoogle_logged());

                MyFavorites myFavorites;
                myFavorites = new MyFavorites();
                myFavorites.execute();

                MyHistory myHistory;
                myHistory = new MyHistory();
                myHistory.execute();

                MyReviews myReviews;
                myReviews = new MyReviews();
                myReviews.execute();

                MainActivity.logged_in = true;
                Intent MainActivity = new Intent(getApplicationContext(), MainActivity.class);
                MainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(MainActivity);

            } else {
                Log.d("Exception: ", msg);
            }

        }
    }
}