package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.asynctask.MyReviews;
import com.museumfinder.evag.findyourmuseum.adapters.ReviewsAdapter;
import com.museumfinder.evag.findyourmuseum.items.ReviewItem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;
import static com.museumfinder.evag.findyourmuseum.MainActivity.logged_in;
import static com.museumfinder.evag.findyourmuseum.MainActivity.user;


/**
 * A simple {@link Fragment} subclass.
 */
public class Reviews extends Fragment {

    View myInflatedView;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";

    private String star;
    private String review;

    public Reviews() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myInflatedView = inflater.inflate(R.layout.reviews_layout, container, false);

        TextView ratingNumber = (TextView) myInflatedView.findViewById(R.id.rating_number);
        ratingNumber.setText(Double.toString(place.getRating()));

        final RatingBar ratingBar = (RatingBar) myInflatedView.findViewById(R.id.rating_bar);
        ratingBar.setRating((float) place.getRating());

        TextView num_reviews = (TextView) myInflatedView.findViewById(R.id.num_reviews);
        num_reviews.setText(place.getNum_reviews() + " reviews");

        mRecyclerView = (RecyclerView) myInflatedView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ReviewsAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);

        final RatingBar submitRating = (RatingBar) myInflatedView.findViewById(R.id.rating_input);

        FloatingActionButton button = (FloatingActionButton) myInflatedView.findViewById(R.id.button);


        submitRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                star = String.valueOf(v);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText review_input;

                if (v.getId() == R.id.button) {

                    review_input = (EditText) myInflatedView.findViewById(R.id.review_input);

                    review = review_input.getText().toString();


                    /*Log.i("review submit:", star);*/
                    Log.i("review submit:", review);

                    if (star == null) {
                        Toast.makeText(getContext(),
                                "Rating is empty!",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if(review.isEmpty()){
                        Toast.makeText(getContext(),
                                "Review is empty!",
                                Toast.LENGTH_SHORT).show();
                        submitRating.setRating(0F);
                    }
                    else{
                        Reviews.AddReviewToDb addReviewToDb;
                        addReviewToDb = new AddReviewToDb();
                        addReviewToDb.execute();
                    }

                }
            }
        });

        return myInflatedView;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((ReviewsAdapter) mAdapter).setOnItemClickListener(new ReviewsAdapter
                .MyClickListener() {

            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });
    }

    private ArrayList<ReviewItem> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < place.getReviews().size(); index++) {
            ReviewItem obj;
            obj = new ReviewItem(place.getReviews().get(index).getName() +
                    " " + place.getReviews().get(index).getSurname().substring(0, 1) + ".",
                    place.getReviews().get(index).getRating(),
                    place.getReviews().get(index).getSubmission_date(), place.getReviews().get(index).getText_review());
            results.add(index, obj);
        }
        return results;
    }

    class AddReviewToDb extends AsyncTask<String, String, String> {

        String msg = "review submit failed";

        @Override
        protected String doInBackground(String... strings) {

            try {

                if (!logged_in){
                    return "not logged in";
                }

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("id",  user.getId());
                params.put("rating", star);
                params.put("submission_date", getCurrentDate());
                params.put("text_review", review);
                params.put("museums_idmuseums", place.getId());


                String link = "http://" + MainActivity.host + "/my_museums/insert_review.php";

                URL url = new URL(link);


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0;) {
                    System.out.print((char) c);
                }

                return "Inserted into db successfully";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("Inserted into db successfully")) {

                Log.i("Reviews:", "Insert into db new entry");

                MyReviews myReviews;
                myReviews = new MyReviews();
                myReviews.execute();

                Toast.makeText(getContext(), "Review submitted successfully", Toast.LENGTH_SHORT).show();

                Intent MainActivity = new Intent(getActivity(), MainActivity.class);
                MainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(MainActivity);

            }else if(msg.equals("not logged in")){
                Toast.makeText(getContext(), "You have to be logged in!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Review submission failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date date = new Date();
        System.out.println(dateFormat.format(date));

        return dateFormat.format(date).toString();
    }

}
