package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData;
import com.museumfinder.evag.findyourmuseum.R;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;


/**
 * A simple {@link Fragment} subclass.
 */
public class HowToAccess extends Fragment implements OnMapReadyCallback {

    View myInflatedView;

    public HowToAccess() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myInflatedView = inflater.inflate(R.layout.how_to_access_layout, container, false);

        TextView address = (TextView) myInflatedView.findViewById(R.id.address);
        address.setText(place.getAddress());

        TextView access_by_car_input = (TextView) myInflatedView.findViewById(R.id.access_by_car_input);
        TextView access_by_transport_input = (TextView) myInflatedView.findViewById(R.id.access_by_transport_input);
        TextView access_on_foot = (TextView) myInflatedView.findViewById(R.id.access_on_foot_input);

        if (place.isFromGoogle()) {
            access_by_car_input.setText(R.string.not_available_info);

            access_by_transport_input.setText(R.string.not_available_info);

            access_on_foot.setText(R.string.not_available_info);
        }
        else {
            access_by_car_input.setText(GetNearbyPlacesData.place.getAccessMuseum().getAccess_by_car());

            access_by_transport_input.setText(GetNearbyPlacesData.place.getAccessMuseum().getAccess_by_transport());

            access_on_foot.setText(GetNearbyPlacesData.place.getAccessMuseum().getAccess_on_foot());
        }

        return myInflatedView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng marker = new LatLng(Double.parseDouble(place.getLat()), Double.parseDouble(place.getLng()));

        googleMap.moveCamera((CameraUpdateFactory.newLatLngZoom(marker,15)));

        googleMap.addMarker(new MarkerOptions().title(place.getName()).position(marker));
        googleMap.getUiSettings().setScrollGesturesEnabled(false);

    }

}
