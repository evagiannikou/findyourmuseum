package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.museumfinder.evag.findyourmuseum.DetailsActivity;
import com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData;
import com.museumfinder.evag.findyourmuseum.GridItem;
import com.museumfinder.evag.findyourmuseum.Photo;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.adapters.GridViewAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Gallery extends Fragment {

    private static final String TAG = "Hi:";
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;
    private String FEED_URL = "http://javatechig.com/?json=get_recent_posts&count=45";

    public Gallery() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View myInflatedView = inflater.inflate(R.layout.gallery_layout, container,false);


        mGridView = (GridView) myInflatedView.findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) myInflatedView.findViewById(R.id.progressBar);

        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(getContext(),R.layout.grid_item_layout, mGridData);
        mGridView.setAdapter(mGridAdapter);

        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                GridItem item = (GridItem) parent.getItemAtPosition(position);
                Intent i = new Intent(getActivity().getApplicationContext(), DetailsActivity.class);
                i.putExtra("image", item.getImage());
                startActivity(i);
            }
        });

        return myInflatedView;
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
            } else {
                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }
    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        Photo[] photos = GetNearbyPlacesData.place.getPhotos();

        String url = "https://maps.googleapis.com/maps/api/place/photo?";
        String key = "key=AIzaSyDzm3B-TRzit_TYtKrmxP-EiokajlnwicI";
        String sensor = "sensor=true";
        String maxWidth="maxwidth=510";
        String maxHeight = "maxheight=510";
        url = url + "&" + key + "&" + sensor + "&" + maxWidth + "&" + maxHeight;

        GridItem item;
        for (int i = 0; i < photos.length; i++) {
            String photoReference = "photoreference="+ photos[i].getPhoto_reference();

            String complete_url = url + "&" + photoReference;
            item = new GridItem();
            item.setImage(complete_url);

            mGridData.add(item);
        }
    }

}
