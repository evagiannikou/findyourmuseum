package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.EventItem;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.adapters.EventAdapter;

import java.util.ArrayList;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;


/**
 * A simple {@link Fragment} subclass.
 */
public class Events extends Fragment {

    View myInflatedView;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";


    public Events() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myInflatedView = inflater.inflate(R.layout.events_layout, container,false);

        mRecyclerView = (RecyclerView) myInflatedView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new EventAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);

        return myInflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((EventAdapter) mAdapter).setOnItemClickListener(new EventAdapter
                .MyClickListener() {

            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });
    }

    private ArrayList<EventItem> getDataSet() {
        ArrayList results = new ArrayList<>();

        for (int index = 0; index < place.getEvents().size(); index++) {
            EventItem obj;
            obj = new EventItem(place.getEvents().get(index).getTitle(),
                    place.getEvents().get(index).getDate(), place.getEvents().get(index).getDescription(), place.getEvents().get(index).getLink());
            results.add(index, obj);
        }
        return results;
    }
}
