package com.museumfinder.evag.findyourmuseum.objects;

/**
 * Created by egiannikou on 21-Dec-17.
 */

public class MyHistoryObj {
    private String museum_name;
    private String museum_id;
    private String address;
    private String added;

    public String getMuseum_name() {
        return museum_name;
    }

    public void setMuseum_name(String museum_name) {
        this.museum_name = museum_name;
    }

    public String getMuseum_id() {
        return museum_id;
    }

    public void setMuseum_id(String museum_id) {
        this.museum_id = museum_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }
}
