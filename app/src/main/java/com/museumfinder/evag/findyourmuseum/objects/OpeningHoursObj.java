package com.museumfinder.evag.findyourmuseum.objects;

/**
 * Created by egiannikou on 13-Dec-17.
 */

public class OpeningHoursObj {
    String day;
    String time;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
