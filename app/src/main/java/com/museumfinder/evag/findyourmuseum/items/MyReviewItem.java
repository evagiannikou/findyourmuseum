package com.museumfinder.evag.findyourmuseum.items;

/**
 * Created by Eva G on 2/5/2017.
 */

public class MyReviewItem {

    private String review_placeid;
    private String review_title;
    private float review_rating;
    private String reviewDate;
    private String reviewDetails;

    public MyReviewItem(String review_placeid, String review_title, float review_rating, String reviewDate, String reviewDetails) {
        this.review_placeid = review_placeid;
        this.review_title = review_title;
        this.review_rating = review_rating;
        this.reviewDate = reviewDate;
        this.reviewDetails = reviewDetails;
    }

    public String getReview_placeid() {
        return review_placeid;
    }

    public void setReview_placeid(String review_placeid) {
        this.review_placeid = review_placeid;
    }

    public String getReview_title() {
        return review_title;
    }

    public void setReview_title(String review_title) {
        this.review_title = review_title;
    }

    public float getReview_rating() {
        return review_rating;
    }

    public void setReview_rating(float review_rating) {
        this.review_rating = review_rating;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDetails() {
        return reviewDetails;
    }

    public void setReviewDetails(String reviewDetails) {
        this.reviewDetails = reviewDetails;
    }
}
