package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Created by egiannikou on 29-May-17.
 */

import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.EventItem;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.GeneralInfo;

public class EventAdapter extends RecyclerView
        .Adapter<EventAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "EventAdapter";
    private ArrayList<EventItem> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView title;
        TextView date;
        TextView details;
        TextView link;

        public DataObjectHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.event_title);
            date = (TextView) itemView.findViewById(R.id.event_date);
            details = (TextView) itemView.findViewById(R.id.event_details);
            link = (TextView) itemView.findViewById(R.id.event_link);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public EventAdapter(ArrayList<EventItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.events_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.title.setText(mDataset.get(position).getEventTitle());
        holder.date.setText(mDataset.get(position).getEventDate());
        holder.details.setText(mDataset.get(position).getEventDetails());

        holder.link.setText(mDataset.get(position).getEventLink());
        Linkify.addLinks(holder.link  , Linkify.WEB_URLS);
        GeneralInfo.stripUnderlines(holder.link);
    }

    public void addItem(EventItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}