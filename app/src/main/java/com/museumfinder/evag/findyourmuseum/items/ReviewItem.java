package com.museumfinder.evag.findyourmuseum.items;

/**
 * Created by egiannikou on 29-May-17.
 */

public class ReviewItem {
    private String reviewer;
    private float review_rating;
    private String reviewDate;
    private String reviewDetails;

    public ReviewItem(String title, float rating, String date, String details) {
        reviewer = title;
        review_rating = rating;
        reviewDate = date;
        reviewDetails = details;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public float getReview_rating() {
        return review_rating;
    }

    public void setReview_rating(float review_rating) {
        this.review_rating = review_rating;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDetails() {
        return reviewDetails;
    }

    public void setReviewDetails(String reviewDetails) {
        this.reviewDetails = reviewDetails;
    }
}