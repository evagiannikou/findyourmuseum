package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.R;

import java.util.Calendar;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;
import static com.museumfinder.evag.findyourmuseum.R.color.green;
import static com.museumfinder.evag.findyourmuseum.R.color.red;
import static com.museumfinder.evag.findyourmuseum.R.id;
import static com.museumfinder.evag.findyourmuseum.R.layout;


/**
 * A simple {@link Fragment} subclass.
 */
public class OpeningHours extends Fragment {

    public OpeningHours() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myInflatedView = inflater.inflate(layout.opening_hours_layout, container,false);

        TextView current_state = (TextView) myInflatedView.findViewById(id.current_state);

        TextView monday = (TextView) myInflatedView.findViewById(id.monday_time);
        TextView tuesday = (TextView) myInflatedView.findViewById(id.tuesday_time);
        TextView wednesday = (TextView) myInflatedView.findViewById(id.wednesday_time);
        TextView thursday = (TextView) myInflatedView.findViewById(id.thursday_time);
        TextView friday = (TextView) myInflatedView.findViewById(id.friday_time);
        TextView saturday = (TextView) myInflatedView.findViewById(id.saturday_time);
        TextView sunday = (TextView) myInflatedView.findViewById(id.sunday_time);

        if(place.isFromGoogle()){

            current_state.setText(R.string.not_available_info);
            monday.setText(R.string.na);
            tuesday.setText(R.string.na);
            wednesday.setText(R.string.na);
            thursday.setText(R.string.na);
            friday.setText(R.string.na);
            saturday.setText(R.string.na);
            sunday.setText(R.string.na);

        }
        else{
            if (place.getOpen_now().equals("true")){
                current_state.setText(getString(R.string.open_noew) + getTodaysOpeningHours());
                current_state.setTextColor(getResources().getColor(green));
            }
            else if (place.getOpen_now().equals("-NA-")){
                current_state.setText(R.string.not_available_info);
            }
            else{
                current_state.setText(R.string.closed_now);
                current_state.setTextColor(getResources().getColor(red));
            }

            monday.setText(place.getOpeningHours().get(0).getTime());
            tuesday.setText(place.getOpeningHours().get(1).getTime());
            wednesday.setText(place.getOpeningHours().get(2).getTime());
            thursday.setText(place.getOpeningHours().get(3).getTime());
            friday.setText(place.getOpeningHours().get(4).getTime());
            saturday.setText(place.getOpeningHours().get(5).getTime());
            sunday.setText(place.getOpeningHours().get(6).getTime());
        }

        return myInflatedView;
    }

    private String getTodaysOpeningHours() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        return place.getOpeningHours().get(day).getTime();
    }

}
