package com.museumfinder.evag.findyourmuseum;

/**
 * Created by egiannikou on 07-Nov-17.
 */

public class GridItem {
    private String image;

    public GridItem() {
        super();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
