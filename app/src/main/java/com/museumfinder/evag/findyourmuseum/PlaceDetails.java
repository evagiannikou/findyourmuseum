package com.museumfinder.evag.findyourmuseum;

import com.museumfinder.evag.findyourmuseum.objects.EventsObj;
import com.museumfinder.evag.findyourmuseum.objects.OpeningHoursObj;
import com.museumfinder.evag.findyourmuseum.objects.ReviewsObj;
import com.museumfinder.evag.findyourmuseum.objects.TicketPricesObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by egiannikou on 19-Oct-17.
 */

public class PlaceDetails {

    private String id;
    private String name;
    private String description;
    private String icon;
    private String vicinity;
    private String open_now;
    private String lat;
    private String lng;
    private String address;
    private String phone;
    private String website;
    private String url;
    private boolean fromGoogle;
    private Photo[] photos;
    private AccessMuseum accessMuseum;
    private List<TicketPricesObj> tickets;
    private List<OpeningHoursObj> openingHours;
    private List<EventsObj> events;
    private List<ReviewsObj> reviews;
    private double rating;
    private int num_reviews;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getOpen_now() {
        return open_now;
    }

    public void setOpen_now(String open_now) {
        this.open_now = open_now;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFromGoogle() {
        return fromGoogle;
    }

    public void setFromGoogle(boolean fromGoogle) {
        this.fromGoogle = fromGoogle;
    }

    public Photo[] getPhotos() {
        return photos;
    }

    public void setPhotos(String pictures) throws JSONException {

        JSONArray gallery = new JSONArray(pictures);
        photos = new Photo[gallery.length()];

        /*System.out.println("photos:" + gallery);*/
        for (int i = 0 ; i < gallery.length() ; i++){

            JSONObject object = gallery.getJSONObject(i);
            Photo photo = new Photo();
            photo.setHeight(object.getInt("height"));
            photo.setWidth(object.getInt("width"));
            photo.setPhoto_reference(object.getString("photo_reference"));

            photos[i] = photo;
            /*System.out.println("photo:" + photo.getHeight() + "| " + photo.getWidth() + "| " + photo.getPhoto_reference());*/
        }


    }

    public AccessMuseum getAccessMuseum() {
        return accessMuseum;
    }

    public void setAccessMuseum(AccessMuseum accessMuseum) {
        this.accessMuseum = accessMuseum;
    }

    public List<TicketPricesObj> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketPricesObj> tickets) {
        this.tickets = tickets;
    }

    public List<OpeningHoursObj> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<OpeningHoursObj> openingHours) {
        this.openingHours = openingHours;
    }

    public List<EventsObj> getEvents() {
        return events;
    }

    public void setEvents(List<EventsObj> events) {
        this.events = events;
    }

    public List<ReviewsObj> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewsObj> reviews) {
        this.reviews = reviews;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getNum_reviews() {
        return num_reviews;
    }

    public void setNum_reviews(int num_reviews) {
        this.num_reviews = num_reviews;
    }
}
