package com.museumfinder.evag.findyourmuseum;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by egiannikou on 07-Nov-17.
 */

public class DetailsActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_view);


        String image = getIntent().getStringExtra("image");

        imageView = (ImageView) findViewById(R.id.grid_item_image);

        Picasso.with(this).load(image).into(imageView);
    }
}
