package com.museumfinder.evag.findyourmuseum;

/**
 * Created by egiannikou on 11-Dec-17.
 */

public class AccessMuseum {
    private String access_by_car;
    private String access_by_transport;
    private String access_on_foot;

    public String getAccess_by_car() {
        return access_by_car;
    }

    public void setAccess_by_car(String access_by_car) {
        this.access_by_car = access_by_car;
    }

    public String getAccess_by_transport() {
        return access_by_transport;
    }

    public void setAccess_by_transport(String access_by_transport) {
        this.access_by_transport = access_by_transport;
    }

    public String getAccess_on_foot() {
        return access_on_foot;
    }

    public void setAccess_on_foot(String access_on_foot) {
        this.access_on_foot = access_on_foot;
    }
}
