package com.museumfinder.evag.findyourmuseum.items;

/**
 * Created by Eva G on 2/5/2017.
 */

public class MyFavoritesItem {

    private String fav_placeid;
    private String fav_title;
    private String fav_city;
    private String fav_distance;

    public MyFavoritesItem(String fav_placeid, String fav_title, String fav_city, String fav_distance) {
        this.fav_placeid = fav_placeid;
        this.fav_title = fav_title;
        this.fav_city = fav_city;
        this.fav_distance = fav_distance;
    }

    public String getFav_placeid() {
        return fav_placeid;
    }

    public void setFav_placeid(String fav_placeid) {
        this.fav_placeid = fav_placeid;
    }

    public String getFav_title() {
        return fav_title;
    }

    public void setFav_title(String fav_title) {
        this.fav_title = fav_title;
    }

    public String getFav_city() {
        return fav_city;
    }

    public void setFav_city(String fav_city) {
        this.fav_city = fav_city;
    }

    public String getFav_distance() {
        return fav_distance;
    }

    public void setFav_distance(String fav_distance) {
        this.fav_distance = fav_distance;
    }
}
