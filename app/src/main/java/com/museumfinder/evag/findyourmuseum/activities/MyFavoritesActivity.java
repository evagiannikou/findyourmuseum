package com.museumfinder.evag.findyourmuseum.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.adapters.MyFavoritesAdapter;
import com.museumfinder.evag.findyourmuseum.asynctask.MyFavorites;
import com.museumfinder.evag.findyourmuseum.fragments.MainFragment;
import com.museumfinder.evag.findyourmuseum.items.MyFavoritesItem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.museumfinder.evag.findyourmuseum.MainActivity.user;

public class MyFavoritesActivity extends AppCompatActivity {

    private RecyclerView.Adapter mAdapter;
    private static String LOG_TAG = "CardViewActivity";

    private static String placeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toolbar toolbar;
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_favorites_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        toolbar.setTitle(Html.fromHtml("<font color='#fffffff'>My favorites</font>"));
        toolbar.setLogo(R.drawable.ic_favorite_white);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyFavoritesAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyFavoritesAdapter) mAdapter).setOnItemClickListener(new MyFavoritesAdapter
                .MyClickListener() {

            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });

        ((MyFavoritesAdapter) mAdapter).setOnItemLongClickListener(new MyFavoritesAdapter
                .MyClickLongListener(){

            @Override
            public void onItemLongClick(final int adapterPosition, View view) {
                Log.i(LOG_TAG, " Clicked on Item Long" + adapterPosition);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MyFavoritesActivity.this);

                // set title
                alertDialogBuilder.setTitle("Delete");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Delete this museum from favorites list?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity
                                ((MyFavoritesAdapter) mAdapter).deleteItem(adapterPosition);

                                placeId = getPlaceId(adapterPosition);

                                MyFavoritesActivity.DeleteMyFavorites deleteMyFavorites;
                                deleteMyFavorites = new MyFavoritesActivity.DeleteMyFavorites();
                                deleteMyFavorites.execute();

                                MyFavorites myFavorites;
                                myFavorites = new MyFavorites();
                                myFavorites.execute();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


    }

    private String getPlaceId(int position){
        return user.getMyFavorites().get(position).getMuseum_id();
    }

    private ArrayList<MyFavoritesItem> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < user.getMyFavorites().size(); index++) {
            MyFavoritesItem obj;

            String address;

            if (user.getMyFavorites().get(index).getAddress().length() > 30){
                address = user.getMyFavorites().get(index).getAddress().substring(0,30);
            }
            else address = user.getMyFavorites().get(index).getAddress();

            obj = new MyFavoritesItem(user.getMyFavorites().get(index).getMuseum_id(), user.getMyFavorites().get(index).getMuseum_name(),
                    address, getDistance(index) + " km away");

            results.add(index, obj);
        }
        return results;
    }

    private String getDistance(int index) {
        double user_lng = MainFragment.latLng.longitude;
        double user_lat = MainFragment.latLng.latitude;
        double place_lng = user.getMyFavorites().get(index).getLng();
        double place_lat = user.getMyFavorites().get(index).getLat();
        
        double theta = user_lng - place_lng;
        double dist = Math.sin(deg2rad(user_lat)) * Math.sin(deg2rad(place_lat)) + Math.cos(deg2rad(user_lat)) * Math.cos(deg2rad(place_lat)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return new DecimalFormat("###.##").format(dist);
    }

    private double rad2deg(double dist) {
        return (dist * 180 / Math.PI);
    }

    private double deg2rad(double user_lat) {
        return (user_lat * Math.PI / 180.0);
    }


    private static class DeleteMyFavorites extends AsyncTask<String, String, String> {

        String msg = "failed deleting fav data from db";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;

            try {
                params = new LinkedHashMap<>();
                params.put("id", user.getId());
                params.put("museum_id", placeId);

                link = "http://" + MainActivity.host + "/my_museums/delete_user_favorites.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    System.out.print((char) c);
                }

                return "successfully delete favorite museum";

            } catch (Exception e) {
                Log.d("Exception:", e.getMessage());
                return msg;
            }
        }
    }
}
