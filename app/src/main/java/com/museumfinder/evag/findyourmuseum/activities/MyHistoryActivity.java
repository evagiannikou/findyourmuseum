package com.museumfinder.evag.findyourmuseum.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.adapters.MyHistoryAdapter;
import com.museumfinder.evag.findyourmuseum.asynctask.MyHistory;
import com.museumfinder.evag.findyourmuseum.items.MyHistoryItem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.museumfinder.evag.findyourmuseum.MainActivity.user;


public class MyHistoryActivity extends AppCompatActivity {

    private RecyclerView.Adapter mAdapter;
    private static String LOG_TAG = "CardViewActivity";

    private static String placeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toolbar toolbar;
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_history_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        toolbar.setTitle(Html.fromHtml("<font color='#fffffff'>My history</font>"));
        toolbar.setLogo(R.drawable.ic_access_time_white);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyHistoryAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyHistoryAdapter) mAdapter).setOnItemClickListener(new MyHistoryAdapter
                .MyClickListener() {

            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });

        ((MyHistoryAdapter) mAdapter).setOnItemLongClickListener(new MyHistoryAdapter
                .MyClickLongListener(){

            @Override
            public void onItemLongClick(final int adapterPosition, View view) {
                Log.i(LOG_TAG, " Clicked on Item Long" + adapterPosition);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MyHistoryActivity.this);

                // set title
                alertDialogBuilder.setTitle("Delete");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Delete this museum from history list?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity
                                ((MyHistoryAdapter) mAdapter).deleteItem(adapterPosition);

                                placeId = getPlaceId(adapterPosition);

                                MyHistoryActivity.DeleteMyHistory deleteMyHistory;
                                deleteMyHistory = new MyHistoryActivity.DeleteMyHistory();
                                deleteMyHistory.execute();

                                MyHistory myHistory;
                                myHistory = new MyHistory();
                                myHistory.execute();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private String getPlaceId(int position){
        return user.getMyHistory().get(position).getMuseum_id();
    }

    private ArrayList<MyHistoryItem> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < user.getMyHistory().size(); index++) {
            MyHistoryItem obj;

            String address;

            if (user.getMyHistory().get(index).getAddress().length() > 30){
                address = user.getMyHistory().get(index).getAddress().substring(0,30);
            }
            else address = user.getMyHistory().get(index).getAddress();

            obj = new MyHistoryItem(user.getMyHistory().get(index).getMuseum_id(), user.getMyHistory().get(index).getMuseum_name(),
                    address, user.getMyHistory().get(index).getAdded());
            results.add(index, obj);
        }
        return results;
    }

    private static class DeleteMyHistory extends AsyncTask<String, String, String> {

        String msg = "failed deleting history data from db";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;

            try {
                params = new LinkedHashMap<>();
                params.put("id", user.getId());
                params.put("museum_id", placeId);

                link = "http://" + MainActivity.host + "/my_museums/delete_user_history.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    System.out.print((char) c);
                }

                return "successfully delete history museum";

            } catch (Exception e) {
                Log.d("Exception:", e.getMessage());
                return msg;
            }
        }
    }
}
