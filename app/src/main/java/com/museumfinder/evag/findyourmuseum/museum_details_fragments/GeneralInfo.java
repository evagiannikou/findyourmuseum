package com.museumfinder.evag.findyourmuseum.museum_details_fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData;
import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.asynctask.MyFavorites;
import com.museumfinder.evag.findyourmuseum.asynctask.MyHistory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.museumfinder.evag.findyourmuseum.GetNearbyPlacesData.place;
import static com.museumfinder.evag.findyourmuseum.MainActivity.logged_in;
import static com.museumfinder.evag.findyourmuseum.MainActivity.user;


/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralInfo extends Fragment {

    View myInflatedView;
    com.github.clans.fab.FloatingActionButton add_to_his;
    com.github.clans.fab.FloatingActionButton add_to_fav;

    public GeneralInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myInflatedView = inflater.inflate(R.layout.general_info_layout, container,false);

        final ImageView imageView = (ImageView) myInflatedView.findViewById(R.id.imageView);

        if (place.isFromGoogle()){
                new DownloadImageTask((ImageView) myInflatedView.findViewById(R.id.imageView))
                        .execute(GetNearbyPlacesData.place.getIcon());

        }
        else{
            String imageDataBytes = GetNearbyPlacesData.place.getIcon().substring(GetNearbyPlacesData.place.getIcon().indexOf(",")+1);
            InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            imageView.setImageBitmap(bitmap);
        }

        TextView description = (TextView) myInflatedView.findViewById(R.id.description);
        description.setText(GetNearbyPlacesData.place.getDescription());

        TextView address = (TextView) myInflatedView.findViewById(R.id.address);
        address.setText(GetNearbyPlacesData.place.getAddress());
        Linkify.addLinks(address  , Linkify.MAP_ADDRESSES);
        stripUnderlines(address);

        TextView phone = (TextView) myInflatedView.findViewById(R.id.phone);
        phone.setText(GetNearbyPlacesData.place.getPhone());
        Linkify.addLinks(phone  , Linkify.PHONE_NUMBERS);
        stripUnderlines(phone);


        TextView website = (TextView) myInflatedView.findViewById(R.id.website);
        website.setText(GetNearbyPlacesData.place.getWebsite());
        Linkify.addLinks(website  , Linkify.WEB_URLS);
        stripUnderlines(website);

        final com.github.clans.fab.FloatingActionMenu menu = (FloatingActionMenu) myInflatedView.findViewById(R.id.menu);
        add_to_fav = (com.github.clans.fab.FloatingActionButton) myInflatedView.findViewById(R.id.menu_item_fav);
        add_to_his = (com.github.clans.fab.FloatingActionButton) myInflatedView.findViewById(R.id.menu_item_hist);


        if ((logged_in) && (user.getMyFavorites().size() > 0)){
            for (int i = 0 ; i < user.getMyFavorites().size(); i++){
                if (user.getMyFavorites().get(i).getMuseum_id().equals(place.getId())){
                    add_to_fav.setLabelText("Added to favorites already");
                    add_to_fav.setEnabled(false);
                }
            }
        }

        if ((logged_in) && (user.getMyHistory().size() > 0)){
            for (int i = 0 ; i < user.getMyHistory().size(); i++){
                if (user.getMyHistory().get(i).getMuseum_id().equals(place.getId())){
                    add_to_his.setLabelText("Added to the visited places already");
                    add_to_his.setEnabled(false);
                }
            }
        }


        add_to_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GeneralInfo.AddFavoritesToDb addFavoritesToDb;
                addFavoritesToDb = new AddFavoritesToDb();
                addFavoritesToDb.execute();

                menu.close(true);
            }
        });

        add_to_his.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GeneralInfo.AddHistoryToDb addHistoryToDb;
                addHistoryToDb = new AddHistoryToDb();
                addHistoryToDb.execute();

                menu.close(true);
            }
        });


        return myInflatedView;
    }

    public static void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    public static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    class AddFavoritesToDb extends AsyncTask<String, String, String> {

        String msg = "favorites submit failed";

        @Override
        protected String doInBackground(String... strings) {

            try {

                if (!logged_in) {
                    return "not logged in";
                }

                Map<String, Object> params = new LinkedHashMap<>();
                params.put("id", user.getId());
                params.put("museums_idmuseums", place.getId());


                String link = "http://" + MainActivity.host + "/my_museums/insert_favorites.php";

                URL url = new URL(link);


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    System.out.print((char) c);
                }

                return "Inserted into db successfully";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("Inserted into db successfully")) {
                add_to_fav.setLabelText("Added to favorites already");
                add_to_fav.setEnabled(false);

                MyFavorites myFavorites;
                myFavorites = new MyFavorites();
                myFavorites.execute();

                Toast.makeText(getContext(), "Added to favorites!", Toast.LENGTH_SHORT).show();
            } else if (msg.equals("not logged in")) {
                Toast.makeText(getContext(), "You have to be logged in!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Favorites submission failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

    class AddHistoryToDb extends AsyncTask<String, String, String> {

        String msg = "history submit failed";

        @Override
        protected String doInBackground(String... strings) {

            try {

                if (!logged_in) {
                    return "not logged in";
                }

                Map<String, Object> params = new LinkedHashMap<>();
                params.put("id", user.getId());
                params.put("museums_idmuseums", place.getId());
                params.put("added", Reviews.getCurrentDate());


                String link = "http://" + MainActivity.host + "/my_museums/insert_history.php";

                URL url = new URL(link);


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    System.out.print((char) c);
                }

                return "Inserted into db successfully";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("Inserted into db successfully")) {
                add_to_his.setLabelText("Added to the visited places already");
                add_to_his.setEnabled(false);

                MyHistory myHistory;
                myHistory = new MyHistory();
                myHistory.execute();

                Toast.makeText(getContext(), "Added to visited museum's list!", Toast.LENGTH_SHORT).show();
            } else if (msg.equals("not logged in")) {
                Toast.makeText(getContext(), "You have to be logged in!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "History submission failed", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
