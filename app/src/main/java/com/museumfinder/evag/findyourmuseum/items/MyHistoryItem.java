package com.museumfinder.evag.findyourmuseum.items;

/**
 * Created by Eva G on 2/5/2017.
 */

public class MyHistoryItem {

    private String his_placeid;
    private String hist_title;
    private String hist_city;
    private String hist_visit;

    public MyHistoryItem(String his_placeid, String hist_title, String hist_city, String hist_visit) {
        this.his_placeid = his_placeid;
        this.hist_title = hist_title;
        this.hist_city = hist_city;
        this.hist_visit = hist_visit;
    }

    public String getHis_placeid() {
        return his_placeid;
    }

    public void setHis_placeid(String his_placeid) {
        this.his_placeid = his_placeid;
    }

    public String getHis_title() {
        return hist_title;
    }

    public void setHis_title(String hist_title) {
        this.hist_title = hist_title;
    }

    public String getHis_city() {
        return hist_city;
    }

    public void setHis_city(String hist_city) {
        this.hist_city = hist_city;
    }

    public String getHis_visit() {
        return hist_visit;
    }

    public void setHis_visit(String hist_visit) {
        this.hist_visit = hist_visit;
    }
}
