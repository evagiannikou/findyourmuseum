package com.museumfinder.evag.findyourmuseum;

/**
 * Created by egiannikou on 29-May-17.
 */

public class EventItem {
    private String eventTitle;
    private String eventDate;
    private String eventDetails;
    private String eventLink;

    public EventItem(String title, String date, String details, String link){
        eventTitle = title;
        eventDate = date;
        eventDetails = details;
        eventLink = link;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(String eventDetails) {
        this.eventDetails = eventDetails;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String eventLink) {
        this.eventLink = eventLink;
    }
}