package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.items.MyHistoryItem;
import com.museumfinder.evag.findyourmuseum.R;

import java.util.ArrayList;

/**
 * Created by egiannikou on 29-May-17.
 */

public class MyHistoryAdapter extends RecyclerView
        .Adapter<MyHistoryAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyHistoryAdapter";
    private ArrayList<MyHistoryItem> mDataset;
    private static MyClickListener myClickListener;
    private static MyClickLongListener myClickLongListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener,View.OnLongClickListener  {
        TextView hist_title;
        TextView hist_city;
        TextView hist_visit;

        public DataObjectHolder(View itemView) {
            super(itemView);
            hist_title = (TextView) itemView.findViewById(R.id.his_title);
            hist_city = (TextView) itemView.findViewById(R.id.his_city);
            hist_visit = (TextView) itemView.findViewById(R.id.his_visit);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View view) {
            myClickLongListener.onItemLongClick(getAdapterPosition(),view);
            return true;
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public void setOnItemLongClickListener(MyClickLongListener myClickLongListener) {
        this.myClickLongListener = myClickLongListener;
    }

    public MyHistoryAdapter(ArrayList<MyHistoryItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_history_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.hist_title.setText(mDataset.get(position).getHis_title());
        holder.hist_city.setText(mDataset.get(position).getHis_city());
        holder.hist_visit.setText(mDataset.get(position).getHis_visit());
    }

    public void addItem(MyHistoryItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }
    public interface MyClickLongListener {
        void onItemLongClick(int adapterPosition, View view);
    }
}