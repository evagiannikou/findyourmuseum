package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.R;
import com.museumfinder.evag.findyourmuseum.items.ReviewItem;

import java.util.ArrayList;

/**
 * Created by egiannikou on 29-May-17.
 */

public class ReviewsAdapter extends RecyclerView
        .Adapter<ReviewsAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "ReviewAdapter";
    private ArrayList<ReviewItem> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView reviewer;
        RatingBar review_rating;
        TextView date;
        TextView review;

        public DataObjectHolder(View itemView) {
            super(itemView);
            reviewer = (TextView) itemView.findViewById(R.id.reviewer);
            review_rating = (RatingBar) itemView.findViewById(R.id.review_rating);
            date = (TextView) itemView.findViewById(R.id.review_date);
            review = (TextView) itemView.findViewById(R.id.review_details);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public ReviewsAdapter(ArrayList<ReviewItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reviews_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.reviewer.setText(mDataset.get(position).getReviewer());
        holder.review_rating.setRating(mDataset.get(position).getReview_rating());
        holder.date.setText(mDataset.get(position).getReviewDate());
        holder.review.setText(mDataset.get(position).getReviewDetails());
    }

    public void addItem(ReviewItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}