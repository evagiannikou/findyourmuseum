package com.museumfinder.evag.findyourmuseum.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.EditText;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.User;

import static com.museumfinder.evag.findyourmuseum.MainActivity.helper;
import static com.museumfinder.evag.findyourmuseum.MainActivity.user_id;

/**
 * Created by egiannikou on 24-Sep-17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "my_museums.db";
    private static final String TABLE_NAME = "users";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SURNAME = "surname";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_FB = "fb_login";
    private static final String COLUMN_GOOGLE = "google_login";

    private static final String INFO_TAG = "info:";

    SQLiteDatabase db;

    private static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME +
            "( " + COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL, " +
            COLUMN_NAME + " TEXT NOT NULL, " +
            COLUMN_SURNAME + " TEXT NOT NULL, " +
            COLUMN_EMAIL + " TEXT NOT NULL, " +
            COLUMN_PASSWORD + " TEXT NOT NULL, " +
            COLUMN_FB + " INTEGER NOT NULL , " +
            COLUMN_GOOGLE + " INTEGER NOT NULL ) ";



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query =  "DROP TABLE IF EXISTS" + TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }


    public String emailExists(String email) {
        db = helper.getReadableDatabase();
        String query = "SELECT " + COLUMN_EMAIL + " FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);

        String db_email;
        String msg = "email doesn't exist";

        if(cursor.moveToFirst()){
            do{
                db_email = cursor.getString(0);
                if (db_email.equals(email)){
                    Log.i(INFO_TAG, "Email exists already");
                    msg = cursor.getString(0);
                    break;
                }
            }
            while(cursor.moveToNext());
        }

        db.close();
        return msg;
    }


    public String searchUser(String email) {
        db = helper.getReadableDatabase();
        String query = "SELECT " + COLUMN_EMAIL + " FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);

        String db_email;
        String msg = "email doesn't exist";

        if(cursor.moveToFirst()){
            do{
                db_email = cursor.getString(0);
                if (db_email.equals(email)){
                    Log.i(INFO_TAG, "Email exists already");
                    msg = cursor.getString(0);
                    break;
                }
            }
            while(cursor.moveToNext());
        }

        db.close();
        return msg;
    }


    public boolean searchPwd(String email, String password) {
        db = helper.getReadableDatabase();
        String query = "SELECT " + COLUMN_PASSWORD + " FROM " + TABLE_NAME + " WHERE " + COLUMN_EMAIL + "= '" + email + "'";
        Cursor cursor = db.rawQuery(query,null);

        String db_password;

        if(cursor.moveToFirst()){
                db_password = cursor.getString(0);
                if (db_password.equals(password)){
                    Log.i(INFO_TAG, "password correct");
                    db.close();
                    return true;
                }
                else {
                    db.close();
                    return false;
                }
        }
        db.close();
        return false;
    }


    public void insertUser(User user) {
        int recordCount = count();

        ContentValues values = new ContentValues();

        values.put(COLUMN_ID, recordCount);
        values.put(COLUMN_NAME, user.getName());
        values.put(COLUMN_SURNAME, user.getSurname());
        values.put(COLUMN_EMAIL, user.getEmail());
        values.put(COLUMN_PASSWORD, user.getpassword());
        values.put(COLUMN_FB, user.getFb_logged());
        values.put(COLUMN_GOOGLE, user.getGoogle_logged());


        db = helper.getWritableDatabase();

        db.insert(TABLE_NAME, null, values);
        db.close();

    }

    public int count(){
        db = helper.getWritableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME;
        int recordCount = db.rawQuery(sql, null).getCount();
        db.close();

        return recordCount;
    }

    public void getUserId(String email) {
        db = helper.getReadableDatabase();
        String query = "SELECT " + COLUMN_ID + " FROM " + TABLE_NAME + " WHERE " + COLUMN_EMAIL + "= '" + email + "'";
        Cursor cursor = db.rawQuery(query,null);


        if(cursor.moveToFirst()){
            String id = cursor.getString(0);
            MainActivity.user_id = Integer.valueOf(id);
        }

        db.close();
    }

    public User getUserData(int user_id) {
        User user = new User();

        db = helper.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + "= '" + user_id + "'";
        Cursor cursor = db.rawQuery(query,null);

        if(cursor.moveToFirst()){
            int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
            String surname = cursor.getString(cursor.getColumnIndex(COLUMN_SURNAME));
            String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
            String password = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
            int fb_logged = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_FB)));
            int google_logged = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_GOOGLE)));

            user.setId(id);
            user.setName(name);
            user.setSurname(surname);
            user.setEmail(email);
            user.setpassword(password);
            user.setFb_logged(fb_logged);
            user.setGoogle_logged(google_logged);

        }

        db.close();
        return user;
    }

    public boolean updateUserData(String new_namestr, String new_surnamestr, String new_password) {
        ContentValues values = new ContentValues();

        values.put("name", new_namestr);
        values.put("surname", new_surnamestr);
        values.put("password", new_password);

        String where = "id = ?";

        String[] whereArgs = { Integer.toString(user_id) };

        SQLiteDatabase db = this.getWritableDatabase();

        boolean updateSuccessful = db.update(TABLE_NAME, values, where, whereArgs) > 0;
        db.close();

        return updateSuccessful;
    }
}
