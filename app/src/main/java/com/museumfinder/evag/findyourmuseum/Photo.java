package com.museumfinder.evag.findyourmuseum;

/**
 * Created by egiannikou on 02-Nov-17.
 */

public class Photo{

    int width;
    int height;
    String photo_reference;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }
}
