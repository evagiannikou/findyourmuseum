package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.items.MyFavoritesItem;
import com.museumfinder.evag.findyourmuseum.R;

import java.util.ArrayList;

/**
 * Created by egiannikou on 29-May-17.
 */

public class MyFavoritesAdapter extends RecyclerView
        .Adapter<MyFavoritesAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyFavoritesAdapter";
    private ArrayList<MyFavoritesItem> mDataset;
    private static MyClickListener myClickListener;
    private static MyClickLongListener myClickLongListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener, View.OnLongClickListener {
        TextView fav_title;
        TextView fav_city;
        TextView fav_distance;

        public DataObjectHolder(View itemView) {
            super(itemView);
            fav_title = (TextView) itemView.findViewById(R.id.fav_title);
            fav_city = (TextView) itemView.findViewById(R.id.fav_city);
            fav_distance = (TextView) itemView.findViewById(R.id.fav_distance);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View view) {
            myClickLongListener.onItemLongClick(getAdapterPosition(),view);
            return true;
        }

    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public void setOnItemLongClickListener(MyClickLongListener myClickLongListener) {
        this.myClickLongListener = myClickLongListener;
    }

    public MyFavoritesAdapter(ArrayList<MyFavoritesItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_favorites_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.fav_title.setText(mDataset.get(position).getFav_title());
        holder.fav_city.setText(mDataset.get(position).getFav_city());
        holder.fav_distance.setText(mDataset.get(position).getFav_distance());
    }

    public void addItem(MyFavoritesItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }

    public interface MyClickLongListener {
        void onItemLongClick(int adapterPosition, View view);
    }

}