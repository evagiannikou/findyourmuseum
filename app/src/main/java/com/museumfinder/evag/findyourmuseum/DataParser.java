package com.museumfinder.evag.findyourmuseum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by egiannikou on 10-Oct-17.
 */

public class DataParser {
    private HashMap<String, String> getPlace(JSONObject googlePlaceJson){
        HashMap<String, String> googlePlacesMap = new HashMap<>();
            String placeName = "-NA-";
            String placeID = "-NA-";
            String vicinity = "-NA-";
            String open_now = "-NA-";
            String latitude;
            String longtitude;
            String reference;


            try {
                if(!googlePlaceJson.isNull("name")){
                   placeName = googlePlaceJson.getString("name");
                }

                if(!googlePlaceJson.isNull("place_id")){
                    placeID = googlePlaceJson.getString("place_id");
                }

                if(!googlePlaceJson.isNull("vicinity")){
                    vicinity = googlePlaceJson.getString("vicinity");
                }

                if(!googlePlaceJson.isNull("opening_hours")){
                    open_now = googlePlaceJson.getJSONObject("opening_hours").getString("open_now");
                }

                latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lat");
                longtitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lng");
                reference = googlePlaceJson.getString("reference");


                googlePlacesMap.put("place_name", placeName);
                googlePlacesMap.put("place_id", placeID);
                googlePlacesMap.put("vicinity", vicinity);
                googlePlacesMap.put("lat", latitude);
                googlePlacesMap.put("lng", longtitude);
                googlePlacesMap.put("reference", reference);
                googlePlacesMap.put("open_now", open_now);
            }catch (JSONException e){
                e.printStackTrace();
            }

            return googlePlacesMap;
    }

    private List<HashMap< String, String>> getPlaces(JSONArray jsonArray){
        int count = jsonArray.length();
        List<HashMap<String, String>> placesList = new ArrayList<>();
        HashMap<String, String> placeMap = null;

        for (int i = 0; i < count ; i++){
            try {
                placeMap = getPlace((JSONObject) jsonArray.get(i));
                placesList.add(placeMap);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return placesList;
    }


    public List<HashMap<String, String>> parse(String jsonData){
        JSONArray jsonArray = null;
        try {
            JSONObject jsonObject = new JSONObject((jsonData));
            jsonArray = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jsonArray);

    }
}
