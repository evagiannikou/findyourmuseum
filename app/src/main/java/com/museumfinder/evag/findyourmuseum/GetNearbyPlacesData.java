package com.museumfinder.evag.findyourmuseum;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.PlaceDetailsJSONParser;
import com.museumfinder.evag.findyourmuseum.objects.EventsObj;
import com.museumfinder.evag.findyourmuseum.objects.OpeningHoursObj;
import com.museumfinder.evag.findyourmuseum.objects.ReviewsObj;
import com.museumfinder.evag.findyourmuseum.objects.TicketPricesObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by egiannikou on 10-Oct-17.
 */

public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

    public static PlaceDetails place;
    private static GoogleMap museumsMap;
    private Context context;
    private String googlePlacesData;

    public GetNearbyPlacesData(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Object... objects) {
        museumsMap = (GoogleMap)objects[0];
        String url = (String)objects[1];

        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            googlePlacesData = downloadUrl.readUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String s) {
        List<HashMap<String, String>> nearbyPlaceList;
        DataParser parser = new DataParser();
        nearbyPlaceList = parser.parse(s);
        showNearbyPlaces(nearbyPlaceList);
    }

    private void showNearbyPlaces(List<HashMap<String, String>> nearbyPlaceList){

        for (int i = 0; i < nearbyPlaceList.size() ; i++){
            MarkerOptions markerOptions = new MarkerOptions();
            final HashMap<String, String> googlePlace = nearbyPlaceList.get(i);

            String placeName = googlePlace.get("place_name");
            String placeID = googlePlace.get("place_id");
            String vicinity = googlePlace.get("vicinity");
            Double lat = Double.parseDouble(googlePlace.get("lat"));
            Double lng = Double.parseDouble(googlePlace.get("lng"));

            LatLng latLng = new LatLng(lat ,lng );
            markerOptions.position(latLng);
            markerOptions.title(placeName);
            markerOptions.snippet(vicinity);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

            Marker mkr = museumsMap.addMarker(markerOptions);
            museumsMap.moveCamera((CameraUpdateFactory.newLatLngZoom(latLng,14)));
            mkr.setTag(placeID);

        }

        museumsMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.i("SELECTED PLACE ID: ", (String) marker.getTag());         // set placeID = getSnippet

                String sb = "https://maps.googleapis.com/maps/api/place/details/json?" +
                "placeid=" + marker.getTag() +
                "&key=AIzaSyAXdpvBY4imMR9rR0yFcivk1ZncdDZynOs";

                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(sb);
            }
        });
    }

    /** A class, to download Google Place Details */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            DownloadUrl downloadUrl = new DownloadUrl();
            try{
                data = downloadUrl.readUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Place Details in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, HashMap<String,String>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected HashMap<String,String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Start parsing Google place details in JSON format
                hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return hPlaceDetails;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(HashMap<String,String> hPlaceDetails){

            place = new PlaceDetails();
            place.setId(hPlaceDetails.get("place_id"));
            place.setName(hPlaceDetails.get("name"));
            place.setIcon(hPlaceDetails.get("icon"));
            place.setVicinity(hPlaceDetails.get("vicinity"));
            place.setOpen_now(hPlaceDetails.get("open_now"));
            place.setLat(hPlaceDetails.get("lat"));
            place.setLng(hPlaceDetails.get("lng"));
            place.setAddress(hPlaceDetails.get("formatted_address"));
            place.setPhone(hPlaceDetails.get("formatted_phone"));
            place.setWebsite(hPlaceDetails.get("website"));
            place.setUrl(hPlaceDetails.get("url"));
            try {
                place.setPhotos(hPlaceDetails.get("photos"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            GetNearbyPlacesData.FindMuseumInDB findMuseumInDB;
            findMuseumInDB = new FindMuseumInDB();
            findMuseumInDB.execute();

        }
    }

    private class FindMuseumInDB extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {

                params = new LinkedHashMap<>();
                params.put("lat", place.getLat());
                params.put("lng", place.getLng());
                params.put("museum_name",place.getName());
                params.put("museum_address",place.getAddress());
                params.put("google_id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_by_lat_lng.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }


                if (sb.toString().length() == 0){
                    Log.i("info:", "No museum found on the DB, get data from google only");
                    return msg = "0";
                }

                JSONObject obj = new JSONObject(sb.toString());

                Log.i("info:", obj.getString("idmuseums"));

                if (obj.getString("fromGoogle").equals("1")){
                    place.setId(obj.getString("idmuseums"));
                    place.setFromGoogle(true);
                }
                else{
                    place.setId(obj.getString("idmuseums"));
                    place.setName(obj.getString("name"));
                    place.setDescription(obj.getString("description"));
                    place.setIcon(obj.getString("main_img"));
                    place.setAddress(obj.getString("address"));
                    place.setPhone(obj.getString("phone"));
                    place.setWebsite(obj.getString("website"));
                    place.setUrl(obj.getString("website"));
                    place.setFromGoogle(false);
                }

                /*get museum how to access*/
                GetNearbyPlacesData.GetHowToAccess getHowToAccess;
                getHowToAccess = new GetHowToAccess();
                getHowToAccess.execute();


                /*get museum tickets*/
                GetNearbyPlacesData.GetTickets getTickets;
                getTickets = new GetTickets();
                getTickets.execute();

                /*get museum opening times*/
                GetNearbyPlacesData.GetOpeningHours getOpeningHours;
                getOpeningHours = new GetOpeningHours();
                getOpeningHours.execute();


                /*get museum events*/
                GetNearbyPlacesData.GetEvents getEvents;
                getEvents = new GetEvents();
                getEvents.execute();


                /*get museum reviews*/
                GetNearbyPlacesData.GetReviews getReviews;
                getReviews = new GetReviews();
                getReviews.execute();

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if ((msg.equals("1") || (msg.equals("0")))) {
                Intent intent = new Intent(context, MuseumDetailsActivity.class);
                context.startActivity(intent);
            }
            else Log.d("Exception: ", s);
        }
    }

    private static class GetHowToAccess extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {
                params = new LinkedHashMap<>();
                params.put("id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_how_to_access.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONObject obj = new JSONObject(sb.toString());

                AccessMuseum accessMuseum = new AccessMuseum();
                accessMuseum.setAccess_by_car(obj.getString("access_by_car"));
                accessMuseum.setAccess_by_transport(obj.getString("access_by_public_transport"));
                accessMuseum.setAccess_on_foot(obj.getString("access_on_foot"));

                place.setAccessMuseum(accessMuseum);

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }
    }

    private static class GetTickets extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {
                params = new LinkedHashMap<>();
                params.put("id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_tickets.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<TicketPricesObj> ticketsList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    TicketPricesObj ticket = new TicketPricesObj();

                    ticket.setType(jsonArray.getJSONObject(i).getString("type"));
                    ticket.setPrice(jsonArray.getJSONObject(i).getString("price"));

                    ticketsList.add(ticket);
                }

                place.setTickets(ticketsList);

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }
    }

    private  static class GetOpeningHours extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {
                params = new LinkedHashMap<>();
                params.put("id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_opening_hours.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<OpeningHoursObj> openingHoursList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    OpeningHoursObj openingHoursObj = new OpeningHoursObj();

                    openingHoursObj.setDay(jsonArray.getJSONObject(i).getString("idopening_hours"));
                    openingHoursObj.setTime(jsonArray.getJSONObject(i).getString("opening_time"));

                    openingHoursList.add(openingHoursObj);
                }

                place.setOpeningHours(openingHoursList);

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }
    }

    private static class GetEvents extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {
                params = new LinkedHashMap<>();
                params.put("id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_events.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<EventsObj> eventsList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    EventsObj eventsObj = new EventsObj();
                    eventsObj.setTitle(jsonArray.getJSONObject(i).getString("title"));
                    eventsObj.setDate(jsonArray.getJSONObject(i).getString("event_date"));
                    eventsObj.setDescription(jsonArray.getJSONObject(i).getString("description"));
                    eventsObj.setLink(jsonArray.getJSONObject(i).getString("link"));

                    eventsList.add(eventsObj);
                }

                place.setEvents(eventsList);

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }
    }

    private static class GetReviews extends AsyncTask<String,String,String>{
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> params;
            String link;
            URL url;
            StringBuilder postData;
            byte[] postDataBytes;
            HttpURLConnection conn;
            BufferedReader reader;
            String line;
            StringBuilder sb;

            try {
                params = new LinkedHashMap<>();
                params.put("id", place.getId());

                link = "http://" + MainActivity.host + "/my_museums/get_museum_reviews.php";

                url = new URL(link);

                postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                postDataBytes = postData.toString().getBytes("UTF-8");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                sb = new StringBuilder();

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<ReviewsObj> reviewsObjList = new ArrayList<>();

                double rating = 0.0;

                for (int i = 0; i < jsonArray.length(); i++) {
                    ReviewsObj reviewsObj = new ReviewsObj();
                    reviewsObj.setId(jsonArray.getJSONObject(i).getInt("id"));
                    reviewsObj.setRating(jsonArray.getJSONObject(i).getInt("rating"));
                    reviewsObj.setSubmission_date(jsonArray.getJSONObject(i).getString("submission_date"));
                    reviewsObj.setText_review(jsonArray.getJSONObject(i).getString("text_review"));
                    reviewsObj.setName(jsonArray.getJSONObject(i).getString("name"));
                    reviewsObj.setSurname(jsonArray.getJSONObject(i).getString("surname"));

                    rating += reviewsObj.getRating();

                    reviewsObjList.add(reviewsObj);
                }

                rating = Math.round(rating/jsonArray.length());

                place.setReviews(reviewsObjList);
                place.setNum_reviews(jsonArray.length());
                place.setRating(rating);

                return msg = "1";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }
    }
}
