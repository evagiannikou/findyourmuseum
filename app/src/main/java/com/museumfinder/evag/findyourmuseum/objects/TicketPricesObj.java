package com.museumfinder.evag.findyourmuseum.objects;

/**
 * Created by egiannikou on 12-Dec-17.
 */

public class TicketPricesObj {
    String type;
    String price;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
