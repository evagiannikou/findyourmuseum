package com.museumfinder.evag.findyourmuseum.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.objects.MyReviewsObj;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.museumfinder.evag.findyourmuseum.MainActivity.user;

/**
 * Created by egiannikou on 28-Dec-17.
 */

public class MyReviews extends AsyncTask<String, String, String> {

    String msg = "failed getting data from db";

    @Override
    protected String doInBackground(String... strings) {

        Map<String, Object> params;
        String link;
        URL url;
        StringBuilder postData;
        byte[] postDataBytes;
        HttpURLConnection conn;
        BufferedReader reader;
        String line;
        StringBuilder sb;

        try {
            params = new LinkedHashMap<>();
            params.put("id", user.getId());

            link = "http://" + MainActivity.host + "/my_museums/get_user_reviews.php";

            url = new URL(link);

            postData = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            postDataBytes = postData.toString().getBytes("UTF-8");

            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            sb = new StringBuilder();

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }

            JSONArray jsonArray = new JSONArray(sb.toString());
            List<MyReviewsObj> myreviewsObjList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                MyReviewsObj reviewsObj = new MyReviewsObj();
                reviewsObj.setMuseum_name(jsonArray.getJSONObject(i).getString("name"));
                reviewsObj.setMuseum_id(jsonArray.getJSONObject(i).getString("idmuseums"));
                reviewsObj.setRating(jsonArray.getJSONObject(i).getInt("rating"));
                reviewsObj.setSubmission_date(jsonArray.getJSONObject(i).getString("submission_date"));
                reviewsObj.setText_review(jsonArray.getJSONObject(i).getString("text_review"));

                myreviewsObjList.add(reviewsObj);
            }

            user.setMyreviews(myreviewsObjList);

            return "success";

        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return msg;
        }
    }

}
