package com.museumfinder.evag.findyourmuseum;

import com.museumfinder.evag.findyourmuseum.objects.MyFavoritesObj;
import com.museumfinder.evag.findyourmuseum.objects.MyHistoryObj;
import com.museumfinder.evag.findyourmuseum.objects.MyReviewsObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by egiannikou on 24-Sep-17.
 */

public class User {

    private int id;
    private int fb_logged;
    private int google_logged;
    private String name;
    private String surname;
    private String email;
    private String password;
    private List<MyReviewsObj> myreviews;
    private List<MyFavoritesObj> myFavorites;
    private List<MyHistoryObj> myHistory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getpassword() {
        return password;
    }

    public void setpassword(String password) {
        this.password = password;
    }

    public int getFb_logged() { return fb_logged; }

    public void setFb_logged(int fb_logged) { this.fb_logged = fb_logged; }

    public int getGoogle_logged() { return google_logged;}

    public void setGoogle_logged(int google_logged) { this.google_logged = google_logged; }

    public String validateInputs(String name, String surname, String email, String pwd) {
        if(name.isEmpty()) return "Name is empty!";
        if (!validateName(name))    return   "Name must include only characters!";

        if(surname.isEmpty()) return "Surname is empty!";
        if (!validateSurname(surname))    return "Surname must include only characters!";

        if(email.isEmpty()) return "Email is empty!";
        if (!validateEmail(email))    return  "Email must be valid!";

        if(pwd.isEmpty()) return "Password is empty!";
        if (!validatePassword(pwd))    return  "Password must be at least 6 characters!";

        return "valid input";
    }

    private boolean validateName(String name) {
        return name.matches("^[a-zA-Z-]+");
    }

    private boolean validateSurname(String surname) {
        return surname.matches("^[a-zA-Z-]+");
    }

    private boolean validateEmail(String email) {
        return email.matches(".*[@].*");
    }

    private boolean validatePassword(String pwd) {
        return pwd.length() >= 6;
    }

    public List<MyReviewsObj> getMyreviews() {
        return myreviews;
    }

    public void setMyreviews(List<MyReviewsObj> myreviews) {
        this.myreviews = myreviews;
    }

    public List<MyFavoritesObj> getMyFavorites() {
        return myFavorites;
    }

    public void setMyFavorites(List<MyFavoritesObj> myFavorites) {
        this.myFavorites = myFavorites;
    }

    public List<MyHistoryObj> getMyHistory() {
        return myHistory;
    }

    public void setMyHistory(List<MyHistoryObj> myHistory) {
        this.myHistory = myHistory;
    }
}
