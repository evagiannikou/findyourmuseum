package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.items.MyReviewItem;
import com.museumfinder.evag.findyourmuseum.R;

import java.util.ArrayList;

/**
 * Created by egiannikou on 29-May-17.
 */

public class MyReviewsAdapter extends RecyclerView
        .Adapter<MyReviewsAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyReviewAdapter";
    private ArrayList<MyReviewItem> mDataset;
    private static MyClickListener myClickListener;
    private static MyClickLongListener myClickLongListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener, View.OnLongClickListener  {
        TextView review_title;
        RatingBar review_rating;
        TextView date;
        TextView review;

        public DataObjectHolder(View itemView) {
            super(itemView);
            review_title = (TextView) itemView.findViewById(R.id.review_title);
            review_rating = (RatingBar) itemView.findViewById(R.id.review_rating);
            date = (TextView) itemView.findViewById(R.id.review_date);
            review = (TextView) itemView.findViewById(R.id.review_details);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View view) {
            myClickLongListener.onItemLongClick(getAdapterPosition(),view);
            return true;
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public void setOnItemLongClickListener(MyReviewsAdapter.MyClickLongListener myClickLongListener) {
        this.myClickLongListener = myClickLongListener;
    }

    public MyReviewsAdapter(ArrayList<MyReviewItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_reviews_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.review_title.setText(mDataset.get(position).getReview_title());
        holder.review_rating.setRating(mDataset.get(position).getReview_rating());
        holder.date.setText(mDataset.get(position).getReviewDate());
        holder.review.setText(mDataset.get(position).getReviewDetails());
    }

    public void addItem(MyReviewItem dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }

    public interface MyClickLongListener {
        void onItemLongClick(int adapterPosition, View view);
    }
}