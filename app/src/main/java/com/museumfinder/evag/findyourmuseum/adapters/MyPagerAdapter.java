package com.museumfinder.evag.findyourmuseum.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.museumfinder.evag.findyourmuseum.museum_details_fragments.Events;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.Gallery;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.GeneralInfo;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.HowToAccess;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.OpeningHours;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.Reviews;
import com.museumfinder.evag.findyourmuseum.museum_details_fragments.Tickets;

/**
 * Created by Eva G on 7/5/2017.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                GeneralInfo generalInfo = new GeneralInfo();
                return generalInfo;
            case 1:
                HowToAccess howToAccess = new HowToAccess();
                return howToAccess;
            case 2:
                Tickets tickets = new Tickets();
                return tickets;
            case 3:
                OpeningHours openingHours = new OpeningHours();
                return openingHours;
            case 4:
                Gallery gallery = new Gallery();
                return gallery;
            case 5:
                Reviews reviews = new Reviews();
                return reviews;
            case 6:
                Events events = new Events();
                return events;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if ( position == 0) return "GENERAL INFO";
        else if (position == 1) return "HOW TO ACCESS";
        else if (position == 2) return "TICKETS";
        else if (position == 3) return "OPENING HOURS";
        else if (position == 4) return "GALLERY";
        else if (position == 5) return "REVIEWS";
        else return "EVENTS";
    }
}

