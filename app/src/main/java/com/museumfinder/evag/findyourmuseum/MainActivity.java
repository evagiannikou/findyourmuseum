package com.museumfinder.evag.findyourmuseum;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.museumfinder.evag.findyourmuseum.activities.LoginActivity;
import com.museumfinder.evag.findyourmuseum.activities.MyFavoritesActivity;
import com.museumfinder.evag.findyourmuseum.activities.MyHistoryActivity;
import com.museumfinder.evag.findyourmuseum.activities.MyProfileActivity;
import com.museumfinder.evag.findyourmuseum.activities.MyReviewsActivity;
import com.museumfinder.evag.findyourmuseum.activities.SignUpActivity;
import com.museumfinder.evag.findyourmuseum.db.DatabaseHelper;
import com.museumfinder.evag.findyourmuseum.fragments.MainFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public static boolean logged_in = false;

    public static int user_id = -1;

    public static DatabaseHelper helper;

    public static String host = "chess.madgik.di.uoa.gr/~egiannikou";

    public static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Toolbar toolbar;
        DrawerLayout drawer;
        ActionBarDrawerToggle toggle;
        NavigationView navigationView;

        super.onCreate(savedInstanceState);

        helper = new DatabaseHelper(this);


        if (!isConnectingToInternet(getApplicationContext())) {
            Log.i("ERROR:","no network available");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Set a title for alert dialog
            builder.setTitle("No internet connection");

            // Ask the final question
            builder.setMessage("Find your museum needs access to the internet. Please turn it on in your device settings.");
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch(which){
                        case DialogInterface.BUTTON_POSITIVE:
                            // User clicked the Yes button
                            Log.i("selected:","Ok");
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            break;
                    }
                }
            };

            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ok", dialogClickListener);
            builder.show();
        }

        if (!isGPSEnabled(getApplicationContext())) {
            Log.i("ERROR:","no location available");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Set a title for alert dialog
            builder.setTitle("No location connection");

            // Ask the final question
            builder.setMessage("Find your museum needs access to the location. Please turn on the location settings in your device settings.");
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch(which){
                        case DialogInterface.BUTTON_POSITIVE:
                            // User clicked the Yes button
                            Log.i("selected:","Ok");
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            break;
                    }
                }
            };

            // Set the alert dialog yes button click listener
            builder.setPositiveButton("Ok", dialogClickListener);
            builder.show();
        }


        if (logged_in){

            setContentView(R.layout.logged_in_activity_main);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logged_in);
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view_logged_in);
            View nav_header = LayoutInflater.from(this).inflate(R.layout.logged_in_header, null);
            nav_header.setMinimumHeight(230);
            ((TextView) nav_header.findViewById(R.id.menu_username)).setText(user.getName() + " " + user.getSurname());
            navigationView.addHeaderView(nav_header);

            navigationView.setNavigationItemSelectedListener(this);
        }
        else{
            setContentView(R.layout.not_logged_in_activity_main);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout_notlogged_in);
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view_notlogged_in);
            navigationView.setNavigationItemSelectedListener(this);
        }


        /* first screen to show*/
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.map_content
                        ,new MainFragment())
                .addToBackStack(null)
                .commit();



    }

    private boolean isGPSEnabled(Context context) {
        LocationManager lm =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isConnectingToInternet(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer;
        if (logged_in)  drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logged_in);
        else drawer = (DrawerLayout) findViewById(R.id.drawer_layout_notlogged_in);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.nav_my_profile_layout) {
            Intent MyProfile = new Intent(this, MyProfileActivity.class);
            startActivity(MyProfile);

        } else if (id == R.id.nav_my_favorites_layout) {
            Intent Myfavorites = new Intent(this, MyFavoritesActivity.class);
            startActivity(Myfavorites);


        } else if (id == R.id.nav_my_history_layout) {
            Intent MyHistory = new Intent(this, MyHistoryActivity.class);
            startActivity(MyHistory);

        } else if (id == R.id.nav_my_reviews_layout) {
            Intent MyReviews = new Intent(this, MyReviewsActivity.class);
            startActivity(MyReviews);

        } else if (id == R.id.login_layout) {
            Intent Login = new Intent(this, LoginActivity.class);
            startActivity(Login);


        } else if (id == R.id.sign_up_layout) {
            Intent SignUp = new Intent(this, SignUpActivity.class);
            startActivity(SignUp);

        }


        DrawerLayout drawer;
        if (logged_in) drawer = (DrawerLayout) findViewById(R.id.drawer_layout_logged_in);
        else drawer = (DrawerLayout) findViewById(R.id.drawer_layout_notlogged_in);

        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_sign_out_layout) {
            MainActivity.logged_in = false;
            Intent MainActivity = new Intent(this, MainActivity.class);
            startActivity(MainActivity);
        }

        return true;
    }
}
