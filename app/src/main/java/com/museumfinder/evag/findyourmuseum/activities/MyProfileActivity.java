package com.museumfinder.evag.findyourmuseum.activities;

import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.museumfinder.evag.findyourmuseum.MainActivity;
import com.museumfinder.evag.findyourmuseum.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class MyProfileActivity extends AppCompatActivity {

    String new_namestr;
    String new_surnamestr;
    String new_pwdstr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toolbar toolbar;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_profile_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        toolbar.setTitle(Html.fromHtml("<font color='#fffffff'>My profile</font>"));
        toolbar.setLogo(R.drawable.ic_person_white);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText name = (EditText) findViewById(R.id.name_input);
        name.setText(MainActivity.user.getName());

        EditText surname = (EditText) findViewById(R.id.surname_input);
        surname.setText(MainActivity.user.getSurname());

        final TextView email = (TextView) findViewById(R.id.email);
        email.setPaintFlags(email.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        email.setText(MainActivity.user.getEmail());

        EditText pwd = (EditText) findViewById(R.id.pwd_new);
        pwd.setText(MainActivity.user.getpassword());

        final Button button = (Button) findViewById(R.id.save_btn);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                if (v.getId() == R.id.save_btn) {

                    EditText new_name = (EditText) findViewById(R.id.name_input);
                    EditText new_surname = (EditText) findViewById(R.id.surname_input);
                    EditText new_password = (EditText) findViewById(R.id.pwd_new);

                    new_namestr = new_name.getText().toString();
                    new_surnamestr = new_surname.getText().toString();
                    new_pwdstr = new_password.getText().toString();


                    /*---------------update user data in DB -----------------------*/

                    MyProfileActivity.UpdateUserData updateUserData;
                    updateUserData = new MyProfileActivity.UpdateUserData();
                    updateUserData.execute();

                }

            }
        });


        if ((MainActivity.user.getFb_logged() == 1) || (MainActivity.user.getGoogle_logged() == 1)) {
            name.setEnabled(false);
            surname.setEnabled(false);
            email.setEnabled(false);
            pwd.setEnabled(false);
            button.setVisibility(View.INVISIBLE);

        }
    }

    class UpdateUserData extends AsyncTask<String, String, String> {
        String msg = "exception";

        @Override
        protected String doInBackground(String... strings) {
            try {

                Map<String, Object> params = new LinkedHashMap<>();
                params.put("id", MainActivity.user_id);
                params.put("name", new_namestr);
                params.put("surname", new_surnamestr);
                params.put("password", new_pwdstr);

                String link = "http://" + MainActivity.host + "/my_museums/update_user_data.php";

                URL url = new URL(link);


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postDataBytes);

                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    System.out.print((char) c);
                }

                return "Updated data into db successfully";

            } catch (Exception e) {
                Log.d("Exception: ", e.getMessage());
                return msg;
            }
        }


        @Override
        protected void onPostExecute(String msg) {
            super.onPostExecute(msg);
            if (msg.equals("Updated data into db successfully")) {
                Log.i("Info: ", "Updated data into db successfully");

                Toast.makeText(getApplicationContext(), "Changes saved", Toast.LENGTH_SHORT).show();

                MainActivity.user.setName(new_namestr);
                MainActivity.user.setSurname(new_surnamestr);
                MainActivity.user.setpassword(new_pwdstr);

            } else {
                Log.d("Exception: ", msg);
            }

        }
    }
}
